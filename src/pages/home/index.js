import React from 'react';
import { Redirect } from 'react-router-dom';
import { pathDashboard } from '../../config/routes';

const Home = () => <Redirect to={pathDashboard} />;

export default Home;
