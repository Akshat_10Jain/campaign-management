import React from 'react';
import { Modal, Calendar} from 'antd';
import styles from './Dashboard.module.scss';

function CalendarModal({
	isModalVisible,
	handleOk,
	handleCancel,
	dateValue,
	handleDateChange,
}) {

  return (
		<Modal 
		 title="Calendar" 
		 visible={isModalVisible} 
		 onOk={handleOk} 
		 closable={false} 
		 okText="Apply" 
		 onCancel={handleCancel} 
		>
			<div className={styles.CalendarDemoCard}>
				<Calendar value={dateValue}  onChange={handleDateChange} fullscreen={false} />
			</div>
		</Modal>
  )
}

export default CalendarModal;