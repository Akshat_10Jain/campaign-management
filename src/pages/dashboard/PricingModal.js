import React from 'react';
import { Modal, Avatar, Button} from 'antd';
import styles from './Dashboard.module.scss';

function PricingModal({
	isPricingModalVisible,
	selectedCampaign,
	handlePricingCancel,
}) {

  return (
		<Modal visible={isPricingModalVisible} footer={null} closable={false}>
			<div className={styles.PricingHeadContent}>
				<div>
					<Avatar shape="square" size={100} src={selectedCampaign.image_url}/>
				</div>
				<div>
				<p className={styles.PricingName}>{selectedCampaign.name}</p>
				<p className={styles.PricingRegion}>{selectedCampaign.region}</p>
				</div>
			</div>
			<div>
				<h2>Pricing</h2>
				{selectedCampaign.price.map((item, index) => {
					return (
					<div className={styles.PricingContent} key={index}>
						<div>{item.time}</div>
						<div>{item.amt}</div>
					</div>
					)
				})}
			</div>
			<div className={styles.PricingFooter}>
				<Button onClick={handlePricingCancel}>Close</Button>
			</div>
	 </Modal>
  )
}

export default PricingModal;