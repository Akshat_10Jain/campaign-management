/* eslint-disable array-callback-return */
import React, { useState, useContext } from 'react';
import moment from 'moment'
import { Layout, Tabs, Avatar } from 'antd';
import {
  BarChartOutlined,
  FileOutlined,
  DollarOutlined,
  CalendarOutlined
} from '@ant-design/icons';
import LanguageContext from '../../components/LanguageContext';
import LanguageChangeButton from '../../components/LanguageChangeButton';
import CustomTable from '../../components/Table';
import CalendarModal from './CalendarModal';
import PricingModal from './PricingModal';
import CampaignData from '../../config/constants';
import styles from './Dashboard.module.scss';
import { languageConverter } from '../../utils/helper';

const { Content } = Layout;

const { TabPane } = Tabs;

function Dashboard() {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isPricingModalVisible, setIsPricingModalVisible] = useState(false);
  const [activeTab, setActiveTab] = useState('1');
  const [dateValue, setDateValue] = useState(moment());
  const [selectedCampaign, setSelectedCampaign] = useState(null);
  const { language } = useContext(LanguageContext);
  const showModal = (campaign) => {
    setIsModalVisible(true);
    setDateValue(moment(campaign.createdOn));
    setSelectedCampaign(campaign);
  };

  const handleOk = () => {
    setIsModalVisible(false);
    selectedCampaign.createdOn = moment(dateValue).format('LL');
    setSelectedCampaign(selectedCampaign);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const showPricingModal = (campaign) => {
    setIsPricingModalVisible(true);
    setSelectedCampaign(campaign);
  };

  const handlePricingCancel = () => {
    setIsPricingModalVisible(false);
  };

  const handleTabChange = (key) => {
    setActiveTab(key)
  }

  const handleDateChange = (value) => {
    setDateValue(value)
  }

  const columns = [
    {
      key: 'createdOn',
      title: `${languageConverter('Date', language.german)}`,
      render({ noOfDays, createdOn }) {
        let text = '';
        if (noOfDays === 0) {
          text = `${languageConverter('Today', language.german)}`
        }
        if (noOfDays < 0) {
          text = `${noOfDays * -1} ${languageConverter('days ago', language.german)}`
        }
        if (noOfDays > 0) {
          text = `${noOfDays} ${languageConverter('days ahead', language.german)}`
        }
        return (
          <div>
            <p className={styles.DateContainer}>{moment(createdOn).format('LL')}
              <span>{text}</span></p>
          </div>
        );
      },
      width: '100px',
    },
    {
      key: 'Camapign',
      title: `${languageConverter('CAMPAIGN', language.german)}`,
      render({ name, region, image_url }) {
        return (
          <div>
            <Avatar shape="square" size={40} src={image_url} /> {name}
            <span className={styles.CampaignDetail}>{region}</span>
          </div>
        );
      },
      width: '120px',
    },
    {
      key: 'view',
      title: `${languageConverter('VIEW', language.german)}`,
      render(campaign) {
        return (
          <>
            <span className={styles.ViewPricing} onClick={() => showPricingModal(campaign)}>
              <DollarOutlined />  View Pricing
         </span>
          </>
        );
      },
      width: '100px',
    },
    {
      title: `${languageConverter('ACTION', language.german)}`,
      render(campaign) {
        return (
          <>
            <span className={styles.Csv}>
              <FileOutlined /> CSV
         </span>
            <span className={styles.Report}>
              <BarChartOutlined /> Report
          </span>
            <span className={styles.Scheduler} onClick={() => showModal(campaign)}>
              <CalendarOutlined /> Schedule Again
          </span>
          </>
        );
      },
      width: '200px',
    },
  ];

  let tableRowData = [];
  if (activeTab === '1') {
    tableRowData = CampaignData.filter((item) => {
      if (moment(item.createdOn) > moment().startOf("day")) {
        item.noOfDays = moment(item.createdOn).diff(moment().startOf("day"), 'days')
        return item
      }
    })
  } else if (activeTab === '2') {
    tableRowData = CampaignData.filter((item) => {
      if (moment(item.createdOn).date() === moment().startOf("day").date()) {
        item.noOfDays = moment(item.createdOn).diff(moment().startOf("day"), 'days')
        return item
      }
    })
  } else {
    tableRowData = CampaignData.filter((item) => {
      if (moment(item.createdOn) < moment().startOf("day")) {
        item.noOfDays = moment(item.createdOn).diff(moment().startOf("day"), 'days')
        return item;
      }
    })
  }
  return (
    <Content
      style={{ margin: '0 16px' }}
    >
      <div className={styles.LanguageContainer}>
        <LanguageChangeButton />
      </div>
      <h1 className={styles.MainHeading}>{languageConverter('Manage Campaigns', language.german)}</h1>
      <div className={styles.content}>
        <Tabs onChange={handleTabChange}>
          <TabPane tab={languageConverter('Upcoming Campaigns', language.german)} key="1">
            <CustomTable
              columnData={columns}
              tableData={tableRowData}
            />
          </TabPane>
          <TabPane tab={languageConverter('Live Camapigns', language.german)} key="2">
            <CustomTable
              columnData={columns}
              tableData={tableRowData}
            />
          </TabPane>
          <TabPane tab={languageConverter('Past Campaigns', language.german)} key="3">
            <CustomTable
              columnData={columns}
              tableData={tableRowData}
            />
          </TabPane>
        </Tabs>
      </div>
      {isModalVisible &&
        <CalendarModal
          isModalVisible={isModalVisible}
          handleOk={handleOk}
          handleCancel={handleCancel}
          dateValue={dateValue}
          handleDateChange={handleDateChange}
        />}
      {isPricingModalVisible &&
        <PricingModal
          isPricingModalVisible={isPricingModalVisible}
          selectedCampaign={selectedCampaign}
          handlePricingCancel={handlePricingCancel}
        />
      }
    </Content>
  )
}

export default Dashboard;