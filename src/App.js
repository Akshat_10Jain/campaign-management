import React, { useState } from 'react';
import { Layout } from 'antd';
import Routes from './Routes';
import LanguageContext from './components/LanguageContext';
import languages from './translate';

const { Content } = Layout;

function App() {
  const [language, setLanguage] = useState('eng');

  const changeLanguage = (ev) => {
    setLanguage(ev === 'eng' ? 'eng' : languages.german);
  };

  return (
    <>
     <LanguageContext.Provider
        value={{ language, changeLanguage }}
        changeLanguage={changeLanguage}
      >
      <Layout>
        <Content>
          <Routes />
        </Content>
      </Layout>
      </LanguageContext.Provider>
    </>
  );
}

export default App;