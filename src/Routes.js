import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { 
 pathHome,
 pathDashboard
} from './config/routes'
import Home from './pages/home';
import Dashboard from './pages/dashboard';
import MainRoute from './components/MainRoute';

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path={pathHome} component={Home} />
      <MainRoute>
        <Switch>
          <Route exact path={pathDashboard} component={Dashboard} />
        </Switch>
      </MainRoute>
    </Switch>
  </Router>
)

export default Routes;