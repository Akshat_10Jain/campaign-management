exports.german = {
    lang: 'german',
    'Upcoming Campaigns': 'Kommende Kampagnen',
    'Live Camapigns':'Live-Kampagne',
    'Past Campaigns':'Vergangene Kampagnen',
    'Manage Campaigns':'Kampagnen verwalten',
    'Date':'Datum',
    'CAMPAIGN':'KAMPAGNE',
    'VIEW':'AUSSICHT',
    'ACTION':'AKTION',
    'days ahead':'Tage voraus',
    'days ago':'Vor Tagen',
    'Today':'Heute'
  };
  