export const languageConverter = (str, langObj) => {
  if (langObj) {
    if (str in langObj) {
      return langObj[str];
    }
    let newStr = '';
    str.split(' ').forEach(item => {
      if (item in langObj) {
        newStr += `${langObj[item]} `;
      } else {
        newStr += `${item} `;
      }
    });
    return newStr;
  }
  return str;
};