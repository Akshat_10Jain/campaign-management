import React from 'react';
import { Layout } from 'antd';
import styles from './TopHeader.module.scss';

const { Header } = Layout;

function TopHeader(){

  return(
    <div>
      <Header>
      <div className={styles.logo}>
        Blue stack
      </div>
      </Header>
    </div>
  );
}

export default TopHeader;