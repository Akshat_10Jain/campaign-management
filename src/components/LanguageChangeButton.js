import React from 'react';
import { Select } from 'antd';
import LanguageContext from './LanguageContext';

const  ChangeLanguageButton = () => {

  return (
    <LanguageContext.Consumer>
      {({ language, changeLanguage }) => (
        <div>
          <span>Select Language</span>
          <Select 
          value={language === 'eng' ? 'eng' : 'german'}  
          onChange={(value) => changeLanguage(value)}
          >
            <Select.Option value="">Select Language</Select.Option>
            <Select.Option value="eng">English</Select.Option>
            <Select.Option value="german">German</Select.Option>
          </Select>
        </div>
      )}
    </LanguageContext.Consumer>
  );
}

export default ChangeLanguageButton;
