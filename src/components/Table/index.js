import React from 'react';
import PropTypes from "prop-types";
import { Table } from 'antd';
import styles from './Table.module.scss';

function CustomTable({columnData, tableData}) {
  return (
    <Table
    tableLayout="auto"
    size="small"
    className={styles.tableContainer}
    columns={columnData} 
    dataSource={tableData}
    bordered
    loading={false}
    pagination={{ 
      hideOnSinglePage: true,
    }}
  />
  )
}

CustomTable.propTypes = {
    columnData: PropTypes.instanceOf(Array),
    tableData: PropTypes.instanceOf(Array),
  };
  
CustomTable.defaultProps = {
    columnData: [],
    tableData: []
};
  
export default CustomTable;