import React from 'react';
import { node } from 'prop-types';
import Main from './Main';

function MainRoute({children}) {
  return (
    <Main>{children}</Main>
  )
}

MainRoute.propTypes = {
  children: node.isRequired
};

export default MainRoute;