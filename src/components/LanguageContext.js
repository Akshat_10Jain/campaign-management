import React from 'react';
import Language from '../translate';

const LanguageContext = React.createContext({
  language: Language.german,
  changeLanguage: () => {},
});

export default LanguageContext;