import React from 'react';
import { Layout } from 'antd';
import {  node } from 'prop-types';
import TopHeader from './topHeader';

const { Content } = Layout;

function Main({children}) {

  return (
        <Layout>
          <Content>
            <Layout>
              <TopHeader />
              {children}
            </Layout>
          </Content>
        </Layout>
  )
}

Main.propTypes = {
  children: node.isRequired,
};

export default Main;