
import moment from 'moment';
import star from '../assets/images/star.jpg'
import fire from '../assets/images/fire-logo.jpg'
import pepsi from '../assets/images/dc.jpg'
import postemarco from '../assets/images/postemarco.png';

const CampaignData = [{
    "key":"1",
    "name": "Test Whatsapp",
    "region": "US",
    "createdOn":  moment('2021-02-10T00:00:00').format('LL'),
    "price": [{
      'time':'1 week - 1 month',
      'amt': '$ 100.00'
    },{
      'time':'1 week - 1 month',
      'amt': '$ 100.00'
    },{
      'time':'1 week - 1 month',
      'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Whatsapp",
    "report": "Some report link for Whatsapp",
    "image_url":star
  },
  {  "key":"2",
    "name": "Super Jewels Quest",
    "region": "CA, FR",
    "createdOn": moment('2021-02-12T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Super Jewels Quest",
    "report": "Some report link for Super Jewels Ques",
    "image_url":fire
  },
  {   "key":"3",
    "name": "Mole Slayer",
    "region": "FR",
    "createdOn": moment('2021-02-02T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Mole Slayer",
    "report": "Some report link for Mole Slayer",
    "image_url":star
  },
  {   "key":"4",
    "name": "Mancala Mix",
    "region": "JP",
    "createdOn": moment('2021-02-27T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Mancala Mix",
    "report": "Some report link for Mancala Mix",
    "image_url":fire
  },
  { "key":"5",
    "name": "Mancala Mix",
    "region": "JP",
    "createdOn": moment('2021-02-27T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Mancala Mix",
    "report": "Some report link for Mancala Mix",
    "image_url":pepsi
  },
  {  "key":"6",
    "name": "Mancala Mix",
    "region": "JP",
    "createdOn": moment('2021-03-01T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Mancala Mix",
    "report": "Some report link for Mancala Mix",
    "image_url":star
  },
  { 
    "key":"7",
    "name": "Mancala Mix",
    "region": "JP",
    "createdOn": moment('2021-02-14T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Mancala Mix",
    "report": "Some report link for Mancala Mix",
    "image_url":fire
  },
  {  "key":"8",
    "name": "Mancala Mix",
    "region": "JP",
    "createdOn": moment('2021-02-13T00:00:00').format('LL'),
    "price": [{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
      },{
        'time':'1 week - 1 month',
        'amt': '$ 100.00'
    }],
    "csv": "Some CSV link for Mancala Mix",
    "report": "Some report link for Mancala Mix",
    "image_url":postemarco
  }

];

export default CampaignData;